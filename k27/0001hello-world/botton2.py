
from kivy.uix.button import Button
 
from kivy.app import App
 
from functools import partial
 
class KivyButton(App):
 
    def disable(self, instance, *args):
 
        instance.disabled = True
 
    def update(self, instance, *args):
 
        instance.text = "Hola"
 
    def build(self):
 
        mybtn = Button(text="Da click en la pantalla",font_size=50, italic=True,background_color=(155,0,51,53))
 
        mybtn.bind(on_press=partial(self.disable, mybtn))
 
        mybtn.bind(on_press=partial(self.update, mybtn))
 
        return mybtn
 
KivyButton().run()

#https://likegeeks.com/es/tutorial-de-kivy
