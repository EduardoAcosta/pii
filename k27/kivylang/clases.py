#coding:utf-8
#main.py
from kivy.app import App
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput

class main_window(App):
        def build(self):
            layout=FloatLayout()
           
            lbl=Label()
            lbl.text="Kivy Lang"    
            lbl.size_hint=None,None
            lbl.x=100
            lbl.y=200

            txt = TextInput()
            txt.size_hint=None,None
            txt.x=200   
            txt.y=300

            layout.add_widget(lbl)
            layout.add_widget(txt)
            return layout

main_window().run()
#app=App()
#app.run()
